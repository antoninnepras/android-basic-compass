package cz.antoninnepras.basic.compass;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity implements SensorEventListener {

    ImageView compassView;
    TextView azimuthTv;
    SensorManager sensorManager;
    Sensor compassSensor;
    int deviceOrientation;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        deviceOrientation = 0;

        compassView = findViewById(R.id.compassImage);
        azimuthTv = findViewById(R.id.azimuthTv);

        sensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        compassSensor = sensorManager.getDefaultSensor(Sensor.TYPE_ROTATION_VECTOR);

    }

    @Override
    public void onSensorChanged(SensorEvent event) {
        float x = event.values[0];
        float y = event.values[1];
        float z = event.values[2];
        float w = event.values[3];

        double sinY = 2.0 * (w * z + x * y);
        double cosY = 1.0 - 2.0 * (y * y + z * z);
        double yaw = Math.atan2(sinY, cosY);

        float rotation = (float) (yaw);

        float rotationDeg = (float) Math.toDegrees(rotation);
        if (rotationDeg < 0) {
            rotationDeg += 360;
        }

        // Change on screen rotation
        deviceOrientation = getWindowManager().getDefaultDisplay().getRotation();

        compassView.setRotation(rotationDeg - 90 * deviceOrientation);
        azimuthTv.setText(getResources().getString(R.string.value, (int) rotationDeg));
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {

    }

    @Override
    protected void onPause() {
        super.onPause();

        sensorManager.unregisterListener(this, compassSensor);
    }

    @Override
    protected void onResume() {
        super.onResume();

        sensorManager.registerListener(this, compassSensor, SensorManager.SENSOR_DELAY_FASTEST);
    }
}